
import JwtDecode from 'jwt-decode'

export default class User {
  static from(token) {
    try {
      let obj = JwtDecode(token)
      return new User(obj)
    } catch (_) {
      return null
    }
  }

  constructor({ username, scopes }) {
    this.username = username
    this.scopes = scopes
  }

  get isAdmin() {
    return this.scopes.includes.admin
  }
}
