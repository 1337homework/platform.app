# Setup nodejs for compiling vue app
FROM node:alpine as node

LABEL authors="Valdas Mazrimas"

RUN apk add --update git && \
  rm -rf /tmp/* /var/cache/apk/*

WORKDIR /app
COPY package*.json /app/

RUN npm install
COPY ./ /app/

ARG env=prod
RUN npm run build -- --prod --environment $env

# Setup nginx reverese proxy for compiled vue app
FROM nginx:alpine

RUN rm -rf /usr/share/nginx/html/*

COPY --from=node /app/dist/ /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]